import pandas as pd
from .models import Record
import tempfile



def convert_excel_to_dict(filename, sheet_name='Sheet1'):
	df = pd.read_excel(filename, sheet_name=sheet_name)
	#strip whitespaces in columns + lowercase
	df.columns = df.columns.str.strip()
	df.columns = map(str.lower, df.columns)
	data = df.to_dict('records')
	return data


def insert_bulk_data(data):
	""" Bulk inserts data(form of dictionary) into database """
	instances = [
		Record(time=a['time'], RER=a['rer'],flow=a['flow'], delta_O2=a['delta o2'],
			delta_CO2=a['delta co2'], raw_O2=a['raw o2'], raw_CO2=a['raw co2'],
			temperature=a['temp'], pressure=a['press'], RH=a['rh']) for a in data
	]
	Record.objects.bulk_create(instances)

def handle_uploaded_file(f):
	tmp = tempfile.mktemp()
	with open(tmp, 'wb') as fp:
		for c in f.chunks():
			fp.write(c)
	return tmp
