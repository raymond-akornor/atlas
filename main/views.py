from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from .models import Record
from .utils import handle_uploaded_file, insert_bulk_data, convert_excel_to_dict
from .forms import UploadFileForm


# Create your views here.

def data(request):
	if request.method == 'GET':
		records = Record.objects.all().values()
		return JsonResponse(list(records), safe=False)

def graph(request):
	return render(request, 'graph.html')

def table(request):
	records = Record.objects.all()
	return render(request, 'table.html', {'records': records})

def index(request):
	return render(request, 'base.html')

@csrf_exempt
def upload_file(request):
	if request.method == 'POST':
		form = UploadFileForm(request.POST, request.FILES)
		if form.is_valid():
			f = request.FILES['file']
			file_path = handle_uploaded_file(f)
			data = convert_excel_to_dict(file_path)
			insert_bulk_data(data)
			return HttpResponseRedirect('/')
	else:
		form = UploadFileForm()
	return render(request, 'upload.html', {'form': form})


