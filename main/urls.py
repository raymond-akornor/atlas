from django.urls import path
from . import views

urlpatterns = [
	path('data', views.data, name='data'),
	path('graph', views.graph, name='graph'),
	path('upload', views.upload_file, name='upload'),
	path('', views.index, name='index'),
	path('table', views.table, name='table'),
]